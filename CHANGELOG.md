# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.0 (2021-04-28)

### Added

* The 7 annotated chromosomes of Lachancea fantastica CBS 6924 (source EBI, [GCA_900074735.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074735.1)).
