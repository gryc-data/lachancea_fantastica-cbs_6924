## *Lachancea fantastica* CBS 6924

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB12929](https://www.ebi.ac.uk/ena/browser/view/PRJEB12929)
* **Assembly accession**: [GCA_900074735.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074735.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: LAFA0
* **Assembly length**: 11,336,659
* **#Chromosomes**: 7
* **Mitochondiral**: No
* **N50 (L50)**: 2,184,418 (3)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5149
* **Pseudogene count**: 90
* **tRNA count**: 201
* **rRNA count**: 10
* **Mobile element count**: 41
